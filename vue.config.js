const devProxy = {
    '/postervideo': {
        target: 'https://mock.yonyoucloud.com/mock/6024/',
        changeOrigin: true,
        // pathRewrite: {
        //     '^/postervideo': ''
        // }
    },
    '/svip': {
        target: 'https://svip-mtool.mty.chinamcloud.com/svip',
        changeOrigin: true,
        pathRewrite: {
            '^/svip': ''
        }
    },
    '/xstream': {
        target: 'http://imo.mtooltest.chinamcloud.cn/VideoLiveCut/xstream',
        // target:'https://acut-mtool.csp.chinamcloud.com/mediaCut',
        // target:'https://mediacut.xctdev.chinamcloud.cn/mediaCut',
        // ws:true,
        changeOrigin: true,
        pathRewrite: {
            '^/xstream': ''
        }
    }
}
const chengduProxy = {

    '/appsocket': {
        target: 'https://mcore.mtooltest.chinamcloud.cn/yumi-svip/svip',
        ws: true,
        wss: true,
        changeOrigin: true,
        pathRewrite: {
            '^/appsocket': ''
        }
    },

    '/svip': {
        // target: 'https://mcore.mtooltest.chinamcloud.cn/yumi-svip/svip',
        target: 'https://console.mtooltest.chinamcloud.cn/yumi-svip/svip',
        // ws:true,
        changeOrigin: true,
        pathRewrite: {
            '^/svip': ''
        }
    },
    '/spider-upload': {
        target: 'https://console.mtooltest.chinamcloud.cn/',
        // target: 'https://console.mtooltest.chinamcloud.cn/spidercrms',

        // target: 'https://console.dev.chinamcloud.cn/spidercrms/auth_upload',

        changeOrigin: true,
        pathRewrite: {
            '^/spider-upload': ''
        },
    },
    '/spider-material': {
        // target: 'https://cloudtoolsplatform-mtool.demo.chinamcloud.cn/spider-material/',
        target: 'https://console.mtooltest.chinamcloud.cn/spidercrms/',
        changeOrigin: true,
        pathRewrite: {
            '^/spider-material': ''
        },
    },
}

module.exports = {
    devServer: {
        // host: 'default.proframe.com',
        disableHostCheck: true,
        port: 8080,
        proxy: chengduProxy, //localProxy, //devProxy,
    },
    css: {
        extract: false
    },
    lintOnSave: false,
};