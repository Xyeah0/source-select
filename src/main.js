import Vue from 'vue'
import App from './App.vue'
import './plugins/axios.js'
import './plugins/element.js'
import 'element-ui/lib/theme-chalk/index.css'


const bus = new Vue();
Vue.prototype.bus = bus
Vue.prototype.$bus = new Vue();

Vue.config.productionTip = false
//dev
if (process.env.NODE_ENV == 'development') {
  // document.cookie = "login_cmc_id=d723c989870936efedd056219c3f81df;path=/";
  // document.cookie = "login_cmc_id=d723c989870936efedd056219c3f81df;path=/";
  //   document.cookie = "login_cmc_tid=bbd6305acc9f8c8fd5bdbeed3470e331;path=/";

  // document.cookie = "login_cmc_id=f366015ceecf0bf71bde675f487be14c;path=/";
  // document.cookie = "login_cmc_tid=77027d8111956c457f9b937df83f2498;path=/";

  document.cookie = "login_cmc_id=e1eab3add6283b48a5d278841d2afb9a;path=/";
  document.cookie = "login_cmc_tid=a74ea9ea87ac9b24affd77e96cf03724;path=/";

}

new Vue({
  render: h => h(App),
}).$mount('#app')
