import AmSourceSelect from './AmSourceSelect.vue'
 
AmSourceSelect.install = function(Vue) {
  Vue.component(AmSourceSelect.name, AmSourceSelect)
}
 
export default AmSourceSelect